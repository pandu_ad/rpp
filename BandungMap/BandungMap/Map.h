//
//  Map.h
//  BandungMap
//
//  Created by Pandu Akbar on 3/7/17.
//  Copyright © 2017 Pandu Akbar. All rights reserved.
//

#ifndef Map_h
#define Map_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

//void CreateMap();
//void CleanMap();
void ** GetPoint(char* point);
char * GetStreetName(void** pnt1, void** pnt2);
void ReadVertices();
void ReadEdges();

#endif /* Map_h */
