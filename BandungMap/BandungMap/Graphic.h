//
//  Graphic.h
//  BandungMap
//
//  Created by Pandu Akbar on 3/6/17.
//  Copyright © 2017 Pandu Akbar. All rights reserved.
//

#ifndef Graphic_h
#define Graphic_h

#include <GLUT/glut.h>
#include <OpenGL/glu.h>
#include <OpenGL/gl.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "Map.h"

extern void **start;
extern void **end;
void renderScene(void);
void init();
void getInput();

#endif /* Graphic_h */
