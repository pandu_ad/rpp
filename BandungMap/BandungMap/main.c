#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <GLUT/glut.h>
#include <OpenGL/glu.h>
#include <OpenGL/gl.h>
#include "Graphic.h"
#include "Map.h"

void getInput(){
    char startChar[64];
    char endChar[64];
    
    printf("Insert starting point: \n");
    fgets(startChar, sizeof(startChar), stdin);
    trim(startChar);
    if(startChar[0] == '#'){
        exit (0);
    }
    
    printf("Insert destination: \n");
    fgets(endChar, sizeof(endChar), stdin);
    trim(endChar);
    if(endChar[0] == '#'){
        exit (0);
    }
    
    start = GetPoint(startChar);
    end = GetPoint(endChar);
    
    if(start == NULL || end == NULL){
        printf("Point cannot be found\n\n");
    }
    else{
        glutPostRedisplay();
    }
}

int main(int argc,char** argv)
{
    // Init Glut
    glutInit(&argc, argv);
    init();
    
    // Get Map
    ReadVertices();
    ReadEdges();
    
    //	Search & print the available route
    
    glutDisplayFunc(renderScene);
//    glutIdleFunc(getInput);
    
    glutMainLoop();
    
    // Clean Data
    
    return 0;
}

