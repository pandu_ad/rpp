//
//  Graphic.c
//  BandungMap
//
//  Created by Pandu Akbar on 3/6/17.
//  Copyright © 2017 Pandu Akbar. All rights reserved.
//

#include "Graphic.h"

void **start;
void **end;

float red=1.0, green=1.0, blue=1.0;

void line(int x1, int y1, int x2, int y2)
{
    glLineWidth(2.5);
    glBegin(GL_LINES);
    glVertex2i(x1,y1);
    glVertex2i(x2,y2);
    glEnd();
}

void drawBitmapText(char *string, int x, int y)
{
    if(string == NULL){
        string = "Empty string";
    }
    
    char *c;
    glRasterPos2f(x, y);
    
    for (c = string; *c != '\0'; c++)
    {
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, *c);
    }
}

bool drawGraph(void **src, void **dest){
    if(src == dest){
        glColor3f(0, 0, 0);
        drawBitmapText(*src, (int)src[1], (int)src[2]);
        return true;
    }
    
    // Search
    int x1 = src[1];
    int y1 = src[2];
    
    bool isFound = false;
    int count = src[3];
    for(int i = 4; i <= 3 + count; i++){
        if(drawGraph(src[i], dest)){
            int x2 = ((void**)src[i])[1];
            int y2 = ((void**)src[i])[2];
            glColor3f(red, 0, blue);
            line(x1, y1, x2, y2);
            glColor3f(0, 0, blue);
            drawBitmapText(GetStreetName(src, src[i]), (x2+x1)/2, (y2+y1)/2);
            isFound = true;
        }
    }
    
    if(isFound){
        glColor3f(0, 0, 0);
        drawBitmapText(*src, x1, y1);
    }
    
    return isFound;
    // Free str
    // free(str);
}

GLuint LoadTexture( const char * filename )
{
    
    GLuint texture;
    
    int width, height;
    
    unsigned char * data;
    
    FILE * file;
    
    file = fopen( filename, "rb" );
    
    if ( file == NULL ) return 0;
    width = 500;
    height = 500;
    data = (unsigned char *)malloc( width * height * 3 );
    //int size = fseek(file,);
    fread( data, width * height * 3, 1, file );
    fclose( file );
    
    for(int i = 0; i < width * height ; i++)
    {
        int index = i*3;
        unsigned char B,R;
        B = data[index];
        R = data[index+2];
        
        data[index] = R;
        data[index+2] = B;
        
    }
    
    
    glGenTextures( 1, &texture );
    glBindTexture( GL_TEXTURE_2D, texture );
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,GL_MODULATE );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST );
    
    
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT );
    gluBuild2DMipmaps( GL_TEXTURE_2D, 3, width, height,GL_RGB, GL_UNSIGNED_BYTE, data );
    free( data );
    
    return texture;
}

void renderScene(void){
//    if(start != NULL && end != NULL){
    glClear (GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    
    GLuint texture;
    texture= LoadTexture( "/Users/user/Dropbox/ITB S2/RPP/Project/BandungMap/Map.bmp" );
    glBindTexture (GL_TEXTURE_2D, texture);
//    glRenderCoolImage () ;
    
    glutSwapBuffers () ;
    
//        if(drawGraph(start, end)){
//            printf("Available path(s) has been printed\n\n");
//        }
//        else{
//            printf("No path found\n\n");
//        }
//        glFlush();
//    }
}
void init(void)
{
    glutInitDisplayMode (GLUT_DEPTH| GLUT_DOUBLE | GLUT_RGBA);
//    glutInitDisplayMode(GLUT_SINGLE |GLUT_RGB);
    glutInitWindowSize (500,500);
    glutInitWindowPosition (100, 100);
    glutCreateWindow ("Map");

//    glClearColor (1.0, 1.0, 1.0, 0.0);
//    glMatrixMode(GL_PROJECTION);
//    glLoadIdentity();
//    gluOrtho2D(0, 500, 0, 500);
}
