//
//  Map.c
//  BandungMap
//
//  Created by Pandu Akbar on 3/7/17.
//  Copyright © 2017 Pandu Akbar. All rights reserved.
//

#include "Map.h"

void **PointList;
void **StreetNames;
int ListLength;

void trim (char *s) {
    int i = strlen(s)-1;
    if ((i >= 0) && (s[i] == '\n'))
        s[i] = '\0';
}

void** AllocNew(int size){
    void **ptr = malloc(size);
    while (!ptr) {
        ptr = malloc(size);
    }
    
    return ptr;
}

void ** GetPoint(char point[]){
    if(point != NULL){
        for(int i = 0; i < ListLength; i++){
            char* name = *(char**)PointList[i];
            if(strcmp(name, point) == 0){
                return PointList[i];
            }
        }
    }
    
    return NULL;
}

char * GetStreetName(void** pnt1, void** pnt2){
    int count = StreetNames[0];
    void **SN;
    for(int i = 1; i <= count; i++){
        SN = StreetNames[i];
        if((SN[1] == pnt1 && SN[2] == pnt2) ||
           (SN[1] == pnt2 && SN[2] == pnt1)){
            return (char*)SN[0];
        }
    }
    return NULL;
}

bool IsPointConnected(void** pnt1, void** pnt2){
    int count = StreetNames[0];
    void **SN;
    for(int i = 1; i <= count; i++){
        SN = StreetNames[i];
        if((SN[1] == pnt1 && SN[2] == pnt2) ||
           (SN[1] == pnt2 && SN[2] == pnt1)){
            return true;
        }
    }
    return false;
}

void ReadVertices(){
    PointList = malloc(5 * sizeof(void**));
    
    int nodeSize = 5 * sizeof(void**);
    
    FILE* stream = fopen("./vertices.csv", "r");
    
    //Get Point
    char line[1024];
    int count = 0;
    while (fgets(line, 1024, stream))
    {
        char* tmp = strdup(line);
        void **vertex = AllocNew(nodeSize);
        
        char* name = strtok(tmp, ",");
        char* x = strtok(NULL, ",");
        char* y = strtok(NULL, ",");
        
        char* sampah;
        vertex[0] = name;
        vertex[1] = strtol(x, &sampah, 10);
        vertex[2] = strtol(y, &sampah, 10);
        PointList[count] = vertex;
        
        count++;
    }
    ListLength = count;
    
    //Assign Point
    char line2[1024];
    stream = fopen("./vertices.csv", "r");
    while (fgets(line2, 1024, stream))
    {
        char* tmp = strdup(line2);
        char* name = strtok(tmp, ",");
        void** vertex = GetPoint(name);
        
        strtok(NULL, ",");
        strtok(NULL, ",");
        
        
        count = 0;
        char* tok;
        for (tok = strtok(NULL, ",");
             tok && *tok;
             tok = strtok(NULL, ",\n"))
        {
            trim(tok);
            if(tok != NULL && strcmp(tok, "") != 0){
                void ** neighbor = GetPoint(tok);
                vertex[4 + count] = neighbor;
                
                count++;
            }
            else{
                strtok(NULL,",");
            }
        }
        vertex[3] = count;
    }
}

void ReadEdges(){
    StreetNames = malloc(10 * sizeof(void**));
    
    int nodeSize = 5 * sizeof(void*);
    
    FILE* stream = fopen("./edges.csv", "r");
    
    //Get Point
    char line[1024];
    int count = 0;
    while (fgets(line, 1024, stream))
    {
        char* tmp = strdup(line);
        
        void** edge = malloc(3 * sizeof(char**));
        edge[0] = strtok(tmp, ",");
        for(int i = 1; i <= 2; i++){
            char* name = strtok(NULL, ",");
            trim(name);
            edge[i] = GetPoint(name);
        }
        StreetNames[1 + count] = edge;
        
        count++;
    }
    StreetNames[0] = count;
}
