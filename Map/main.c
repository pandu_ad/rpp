#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
// #include <GLUT/glut.h>
// #include <OpenGL/glu.h>
// #include <OpenGL/gl.h>
// #include "Graphic.h"
#include "Map.h"
		
FILE* file;
void* UsedPoint[50];

void GetRoute(char* prefix, void **src, void **dest){
    if(src == dest){
        fprintf(file, "%s", prefix);
        fprintf(file, "%s", *dest);
        fprintf(file, "%s", "\n");

        return;
    }
    
    // Build string
    char *str = malloc(1000 * sizeof(char));
	str[0] = '\0';
    strcat(str, prefix);
    strcat(str, *src);
    strcat(str, ",");
	
    // Search
    
    for(int i = 0; i < ListLength; i++) {
		if (strstr(str, *(char**)PointList[i]) == NULL) {
			if(IsPointConnected(src, PointList[i])) {
				GetRoute(str, (void**)PointList[i], dest);
			}
		}
    }
    
    // Free str
    // free(str);
}

void getInput(){
    char startChar[64];
    char endChar[64];
    
    while(true){
        printf("Insert starting point: ");
        fgets(startChar, sizeof(startChar), stdin);
        trim(startChar);
        if(startChar[0] == '#'){
            break;
            // exit (0);
			
        }
        
        printf("Insert destination: ");
        fgets(endChar, sizeof(endChar), stdin);
        trim(endChar);
        if(endChar[0] == '#'){
            break;
            // exit (0);
        }
        
        void **start;
        void **end;
        start = GetPoint(startChar);
        end = GetPoint(endChar);
        
        if(start == NULL || end == NULL){
            printf("Point cannot be found\n\n");
        }
        else{
			file = fopen("route.csv", "w");
		
            GetRoute("", start, end);
			
			fclose(file);
			
			system("./MapGenerator.exe");
            printf("Map has been printed, check the file\n\n");
        }
    }
}

int main(int argc,char** argv)
{
    // Get Map
    ReadVertices();
    ReadEdges();
    
    getInput();
    
    getchar();
}
