//
//  Graphic.c
//  BandungMap
//
//  Created by Pandu Akbar on 3/6/17.
//  Copyright © 2017 Pandu Akbar. All rights reserved.
//

#include "Graphic.h"

void **start;
void **end;

void line(int x1, int y1, int x2, int y2)
{
    glLineWidth(2.5);
    glBegin(GL_LINES);
    glVertex2i(x1,y1);
    glVertex2i(x2,y2);
    glEnd();
}

void drawBitmapText(char *string, int x, int y)
{
    if(string == NULL){
        string = "Empty string";
    }
    
    char *c;
    glRasterPos2f(x, y);
    
    for (c = string; *c != '\0'; c++)
    {
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, *c);
    }
}

void renderScene(void){
    if(start != NULL && end != NULL){
        glClear (GL_COLOR_BUFFER_BIT);
    
        if(drawGraph(start, end)){
            printf("Available path(s) has been printed\n\n");
        }
        else{
            printf("No path found\n\n");
        }
        glFlush();
    }
}

void init(void)
{
    
    glutInitDisplayMode(GLUT_SINGLE |GLUT_RGB);
    glutInitWindowSize (500,500);
    glutInitWindowPosition (100, 100);
    glutCreateWindow ("Map");

    glClearColor (1.0, 1.0, 1.0, 0.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, 500, 0, 500);
}
